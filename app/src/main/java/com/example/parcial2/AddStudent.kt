package com.example.parcial2

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.add_student.*

class AddStudent (context: Context, val name: String, val lastname: String, val num: String, val phone: String, private val callback: (String, String, String, String) -> Unit) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_student)
        nameEdit.setText(name)
        lastNameEdit.setText(lastname)
        numEdit.setText(num)
        phoneEdit.setText(phone)

        save.setOnClickListener {
            makeValidation()

        }
    }

    private fun makeValidation(){

        if (nameEdit.text.isEmpty()|| lastNameEdit.text.isEmpty()|| numEdit.text.isEmpty() || phoneEdit.text.isEmpty()){
            val builder= AlertDialog.Builder(context)
            builder.setMessage("Debe completar todos los campos")
            builder.setPositiveButton("Aceptar"){ _,_ ->
            }
            builder.show()
        }else {
            val name = nameEdit.text.toString()
            val lastname = lastNameEdit.text.toString()
            val num = numEdit.text.toString()
            val phone = phoneEdit.text.toString()
            callback(name, lastname, num, phone)
            dismiss()

        }

    }
}