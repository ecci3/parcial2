package com.example.parcial2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.activity_secondary.*


class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secondary)

        fragment1.setOnClickListener {
            val transaction: FragmentTransaction= supportFragmentManager.beginTransaction()
            transaction.add(R.id.container, Fragment1(), null)
            transaction.commit()
        }

        fragment2.setOnClickListener {
            val transaction: FragmentTransaction= supportFragmentManager.beginTransaction()
            transaction.add(R.id.container, Fragment2(), null)
            transaction.commit()
        }

    }
}