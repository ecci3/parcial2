package com.example.parcial2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment2.*


class Fragment2 : Fragment() {
    var image1=true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        editImage.setOnClickListener {
            image1=!image1
            changeImage()
        }
    }

    private fun changeImage(){
        if(image1){
            imageView.setImageResource(R.drawable.dormir)
        }else{
            imageView.setImageResource(R.drawable.trabajar)
        }

    }
}