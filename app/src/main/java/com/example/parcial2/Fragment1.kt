package com.example.parcial2

import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment1.*


class Fragment1 : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        textEditButton.setOnClickListener {
            makeTimeDialog()
        }
    }

    private fun makeTimeDialog(){
        val dialog= TimePickerDialog(requireContext(), {dialog,hour,minute ->
            textEdit.setText(hour.toString().plus(":").plus(minute.toString()))
        },1, 1, true)
        dialog.show()
    }


}