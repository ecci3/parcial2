package com.example.parcial2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.setup_student.view.*

class SetupStudent (val students: MutableList<Student>, val callback: (Student, Boolean) -> Unit): RecyclerView.Adapter<SetupStudent.ProductViewHolder>() {

    class ProductViewHolder(itemView: View, val callback: (Student, Boolean) -> Unit) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Student) {
            itemView.nameText.text = item.name
            itemView.lastNameText.text = item.lastname
            itemView.documentText.text = item.num
            itemView.phoneText.text = item.phone

            itemView.eraserButton.setOnClickListener {
                callback(item, true)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.setup_student, parent, false)
        return ProductViewHolder(view, callback)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(students[position])
    }

    override fun getItemCount(): Int {
        return students.size
    }

    fun addStudent(student: Student) {
        students.add(student)
    }

    fun deleteStudent(student: Student) {
        students.remove(student)
    }

}