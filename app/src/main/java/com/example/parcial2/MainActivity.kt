package com.example.parcial2

/*Integrantes:
Juan Camilo Bautista Roa - 50119
Deisy Tatiana Gómez Fernández - 74233
Jose Andrés Perea Camelo - 34922*/

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class Student (var name: String, var lastname: String, var num: String, var phone: String)
class MainActivity : AppCompatActivity() {
    private lateinit var adapter: SetupStudent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupButtons()
        setupList()
        button2.setOnClickListener {
            val intent= Intent(this, SecondActivity:: class.java )
            startActivity(intent)
        }
    }

    private fun setupButtons() {
        button.setOnClickListener {
            val dialog = AddStudent(this, "", "", "", "") { name, lastname, document, phone ->
                addStudent(name, lastname, document, phone)
            }
            dialog.show()
        }
    }

    private fun setupList() {
        val students = mutableListOf(
                Student("Nombres", "Apellidos", "Identificacion", "Celular"))

        adapter = SetupStudent(students) { item, isDelete ->
            if(isDelete) deleteStudent(item)

        }
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun addStudent(name: String, lastname: String, document: String, phone: String) {
        val student = Student(name, lastname, document, phone)
        adapter.addStudent(student)
        adapter.notifyDataSetChanged()
    }

    private fun deleteStudent(student: Student) {

        val builder= AlertDialog.Builder(this)
        builder.setMessage("Confirma eliminar la información del estudiante ?")
        builder.setPositiveButton("Aceptar"){ _,_ ->
            adapter.deleteStudent(student)
            adapter.notifyDataSetChanged()

        }
        builder.setNegativeButton("Cancelar") { dialog, position ->
        }
        builder.show()

    }

}
